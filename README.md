<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="src/assets/vercator_logo.png" alt="Logo" width="370">
  </a>
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)

This is my technical test for the Correvate Angular developer position.

### Built With

[![Angular][Angular.io]][Angular-url]
[![Tailwind][Tailwind.com]][Tailwind-url]


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

npm, node
  ```sh
  npm install
  npm start
  ```


<!-- ROADMAP -->
## Roadmap

- [x] Setup project
- [x] Create Unit tests
- [x] Create login page and provide auth middleware to perform API calls
- [x] Create dashboard page to display items in a list
- [ ] Create a detail page and a create new item page


<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Marc Amorós - [my webpage](https://marcamoros.io) - info@marcamoros.io

Project Link: [https://gitlab.com/marks97/correvatetest](https://gitlab.com/marks97/correvatetest)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[product-screenshot]: images/screenshot.png
[Angular.io]: https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white
[Angular-url]: https://angular.io/
[Tailwind.com]: https://img.shields.io/static/v1?style=for-the-badge&message=Tailwind+CSS&color=222222&logo=Tailwind+CSS&logoColor=06B6D4&label=
[Tailwind-url]: https://tailwindui.com/
