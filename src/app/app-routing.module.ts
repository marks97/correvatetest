import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { BlankComponent } from "./layouts/blank/blank.component";
import { AuthGuardService } from "./services/guards/auth-guard.service";
import { DashboardComponent } from "./page/dashboard/dashboard.component";
import { AuthComponent } from "./modules/auth/auth.component";
import { CreatePetComponent } from "./page/create-pet/create-pet.component";

const routes: Routes = [
  {
    path: '',
    component: BlankComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/dashboard', pathMatch: 'full'},
      { path: 'dashboard', component: DashboardComponent },
      { path: 'create', component: CreatePetComponent }
    ]
  },
  { path: 'auth', component: AuthComponent },
  { path: '**', redirectTo: '/dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
