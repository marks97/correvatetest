import { Pipe, PipeTransform } from '@angular/core';
import { Pet } from "../interfaces/domain/pet";

@Pipe({ name: 'petFilter' })
export class PetFilterPipe implements PipeTransform {
  transform(pets: Pet[], filter: string): Pet[] {

    if (!filter) {
      return pets;
    }

    filter = filter.toLocaleLowerCase();

    return pets.filter(pet => {
      return pet.name.toLocaleLowerCase().includes(filter) || pet.category?.name.toLocaleLowerCase().includes(filter)
    });

  }
}
