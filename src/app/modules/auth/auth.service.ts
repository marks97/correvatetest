import { Injectable } from '@angular/core';
import { UserDto } from "../../interfaces/DTO/user.dto";
import { LoginDto } from "../../interfaces/DTO/login.dto";
import { Observable } from "rxjs";
import { Response } from "../../interfaces/API/response";
import { HttpService } from "../../services/http/http.service";
import { environment } from "../../../environments/environment";
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  isAuthenticated(): boolean {
    const sessionStorageToken = sessionStorage.getItem('authToken');
    const localStorageToken = localStorage.getItem('authToken');
    return !!(sessionStorageToken || localStorageToken);
  }

  signup(user: UserDto): Observable<Response> {
    return this.httpService.post({ path: 'user', body: user });
  }

  login(data: LoginDto): Observable<Response> {
    const baseUrl = environment.api.url;
    const path = 'user/login';
    const url = new URL(path, baseUrl);
    url.searchParams.set('username', data.username);
    url.searchParams.set('password', data.password);
    return this.httpService.get({ path: path + url.search });
  }

  signout() {
    this.httpService.get({ path: 'user/logout' }).subscribe(() => {
      localStorage.clear();
      sessionStorage.clear();
      this.router.navigate(['/auth']);
    })
  }

}
