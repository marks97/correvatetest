import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "./auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html'
})
export class AuthComponent {

  public form: FormGroup;
  public submitted = false;

  constructor(
    private router: Router,
    private authService: AuthService,
  ) {
    this.form = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      rememberUser: new FormControl(false),
    });
  }

  async submit() {

    this.submitted = true;

    try {

      this.authService.login({
        username: this.controls['username'].value,
        password: this.controls['password'].value
      }).subscribe(res => {

        const message = res.message;
        const authToken = message.replace('logged in user session:', '');

        if (this.controls['rememberUser'].value) {
          localStorage.setItem('authToken', authToken);
        } else {
          sessionStorage.setItem('authToken', authToken);
        }

        this.router.navigate(['/dashboard']);

      });


    } catch (e) {
      console.log('err');
      console.log(e);
      this.submitted = false;
    }

  }

  get controls(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  get formErrors(): ValidationErrors {
    return this.form.errors || {};
  }

  get isFormValid(): boolean {
    return this.form.status === "VALID";
  }
}
