import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientModule } from "@angular/common/http";

describe('AuthService', () => {

  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientModule] });
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create a user', (done: DoneFn) => {

    const user = service.signup({
      username: 'test',
      password: 'pass',
    });

    user?.subscribe(res => {
      expect(res.code).toBe(200);
      done();
    });

  });

  it ('should login with previously created user', (done: DoneFn) => {

    const token = service.login({
      username: 'test',
      password: 'pass'
    });

    token?.subscribe(res => {
      expect(res.code).toBe(200);
      done();
    });

  });

});
