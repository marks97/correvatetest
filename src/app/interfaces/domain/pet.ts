import { PetStatus } from "../../enums/pet-status";

export interface Pet {
  id: number;
  category: {
    id: number;
    name: string;
  };
  name: string;
  photoUrls: string[];
  tags: PetTag[];
  status: PetStatus;
}

export interface PetTag {
  id: number;
  name: string;
}
