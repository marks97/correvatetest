export interface Response {
  code: number;
  type: string;
  message: string;
}
