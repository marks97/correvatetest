export interface HttpOptions {
  path: string;
  body?: any;
  responseType?: string;
}
