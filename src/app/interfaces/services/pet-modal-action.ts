import { PetActionKey } from "../../enums/pet-action-key";

export interface PetModalAction {
  petId: number
  key: PetActionKey,
}
