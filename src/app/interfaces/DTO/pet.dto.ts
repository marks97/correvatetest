import { PetStatus } from "../../enums/pet-status";
import { PetTag } from "../domain/pet";

export interface PetDto {
  id?: number;
  category?: {
    id: number;
    name: string;
  };
  name: string;
  photoUrls?: string[];
  tags?: PetTag[];
  status: PetStatus;
}
