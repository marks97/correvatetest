import {Component, EventEmitter, Input} from '@angular/core';
import {Pet} from "../../interfaces/domain/pet";
import {PetService} from "../../services/domain/pets/pet.service";
import {BehaviorSubject} from "rxjs";
import {ColorService} from "../../services/utils/color.service";
import {MatDialogRef} from "@angular/material/dialog";
import {PetModalAction} from "../../interfaces/services/pet-modal-action";
import {PetActionKey} from "../../enums/pet-action-key";

@Component({
  selector: 'app-detail-pet',
  templateUrl: './detail-pet.component.html'
})
export class DetailPetComponent {

  private petId?: number;
  public pet?: Pet;

  @Input()
  public petIdObservable: BehaviorSubject<number | undefined> = new BehaviorSubject(this.petId);
  public eventEmitter: EventEmitter<PetModalAction> = new EventEmitter();

  constructor(
    public colorService: ColorService,
    private petService: PetService,
    private dialog: MatDialogRef<DetailPetComponent>
  ) {
    this.petIdObservable.subscribe(id => {
      if (id) {
        this.petId = id;
        this.getPet(id);
      }
    });
  }

  getPet(id: number) {
    this.petService.getPetById(id).subscribe(pet => {
      this.pet = pet;
    });
  }

  close() {
    this.dialog.close();
  }

  next() {
    if (this.petId) {
      this.eventEmitter.emit({petId: this.petId, key: PetActionKey.gotNextPet});
    }
  }

  previous() {
    if (this.petId) {
      this.eventEmitter.emit({petId: this.petId, key: PetActionKey.goPreviousPet});
    }
  }

}
