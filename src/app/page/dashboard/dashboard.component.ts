import {Component} from '@angular/core';
import {Pet} from "../../interfaces/domain/pet";
import {AuthService} from "../../modules/auth/auth.service";
import {PetService} from "../../services/domain/pets/pet.service";
import {PetStatus} from "../../enums/pet-status";
import {BehaviorSubject} from "rxjs";
import {ModalService} from "../../services/utils/modal.service";
import {DetailPetComponent} from "../detail-pet/detail-pet.component";
import {ColorService} from "../../services/utils/color.service";
import {PetActionKey} from "../../enums/pet-action-key";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent {

  public showMenu = false;

  public pets: Pet[] = [];
  public petFilterStr = "";
  public petStatus = PetStatus.available;
  public petStatusObservable: BehaviorSubject<PetStatus> = new BehaviorSubject(this.petStatus);

  constructor(
    public colorService: ColorService,
    private authService: AuthService,
    private petService: PetService,
    private modalService: ModalService,
  ) {
    this.petStatusObservable.subscribe(newStatus => {
      this.petStatus = newStatus;
      this.getPets(newStatus);
    });
  }

  getPets(status: PetStatus) {
    this.petService.getPetsByStatus(status).subscribe(pets => {
      this.pets = pets;
    });
  }

  openPetDetail(id: number) {
    const modal = this.modalService.createModal(DetailPetComponent, ["rounded-lg"]);
    (<DetailPetComponent>modal.componentInstance).petIdObservable.next(id);
    (<DetailPetComponent>modal.componentInstance).eventEmitter.subscribe(action => {
      const selectedPet = this.pets.filter(pet => pet.id === action.petId)[0];
      const petIndex = this.pets.indexOf(selectedPet);
      if (action.key === PetActionKey.gotNextPet) {
        const nextPet = this.pets[petIndex + 1];
        if (nextPet) {
          (<DetailPetComponent>modal.componentInstance).petIdObservable.next(nextPet.id);
        }
      } else if (action.key === PetActionKey.goPreviousPet) {
        const previousPet = this.pets[petIndex - 1];
        if (previousPet) {
          (<DetailPetComponent>modal.componentInstance).petIdObservable.next(previousPet.id);
        }
      }
    })
  }

  signout() {
    this.authService.signout();
  }

  get status() {
    return PetStatus;
  }

}
