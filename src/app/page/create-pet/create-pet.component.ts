import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { PetStatus } from "../../enums/pet-status";
import { PetService } from "../../services/domain/pets/pet.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-create-pet',
  templateUrl: './create-pet.component.html'
})
export class CreatePetComponent {

  public form: FormGroup;
  public formData: FormData;
  public photo: string | null = null;
  public submitted = false;

  constructor(
    private petService: PetService,
    private router: Router,
  ) {
    this.formData = new FormData();
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      status: new FormControl('available', [Validators.required])
    });
  }

  submit() {

    this.submitted = true;

    try {

      this.petService.addPet({
        name: this.controls['name'].value,
        status: this.controls['status'].value
      }).subscribe(pet => {
        if (this.photo) {
          const id = pet.id;
          this.petService.setPetPhoto(id, this.formData).subscribe(res => {
            console.log('res');
            console.log(res);
            if (res.code !== 200) {
              // TODO print error
            }
            this.router.navigate(['/dashboard']);
          });
        } else {
          this.router.navigate(['/dashboard']);
        }
      });
    } catch (e) {
      console.log('err');
      console.log(e);
      this.submitted = false;
    }
  }

  async updatePhoto(event: any) {

    const target = event.target as HTMLInputElement;
    if (target && target.files) {
      const file: File = target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      this.photo = file.name;
      if (file) {
        this.formData.append('file', file);
      }
    }

  }

  get controls(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  get formErrors(): ValidationErrors {
    return this.form.errors || {};
  }

  get isFormValid(): boolean {
    return this.form.status === "VALID";
  }

  get status() {
    return PetStatus;
  }

}
