import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { BlankComponent } from './layouts/blank/blank.component';
import { DashboardComponent } from './page/dashboard/dashboard.component';
import { MaterialModule } from "./modules/material/material.module";
import { AuthModule } from "./modules/auth/auth.module";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { AuthInterceptor } from "./services/interceptors/auth.interceptor";
import { FormsModule} from "@angular/forms";
import { PetFilterPipe } from "./pipes/petFilter.pipe";
import { CreatePetComponent } from './page/create-pet/create-pet.component';
import { DetailPetComponent } from './page/detail-pet/detail-pet.component';
import {ModalService} from "./services/utils/modal.service";

@NgModule({
  declarations: [
    AppComponent,
    BlankComponent,
    DashboardComponent,
    PetFilterPipe,
    CreatePetComponent,
    DetailPetComponent,
  ],
  imports: [
      BrowserModule,
      NoopAnimationsModule,
      AppRoutingModule,
      HttpClientModule,
      AuthModule,
      MaterialModule,
      FormsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
