import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { HttpOptions } from "../../interfaces/API/http-options";
import { Observable } from "rxjs";
import { HttpType } from "../../enums/http-type";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private API_BASE_URL = environment.api.url;

  constructor(
    private httpClient: HttpClient
  ) { }

  get<T>(options: HttpOptions): Observable<T> {
    return this.httpCall(HttpType.GET, options);
  }

  delete<T>(options: HttpOptions): Observable<T> {
    return this.httpCall(HttpType.DELETE, options);
  }

  post<T>(options: HttpOptions): Observable<T> {
    return this.httpCall(HttpType.POST, options);
  }

  private httpCall<T>(type: HttpType, options: HttpOptions): Observable<T> {
    return this.httpClient.request<T>(type, this.API_BASE_URL + options.path, {
      body: options.body
    });
  }


}
