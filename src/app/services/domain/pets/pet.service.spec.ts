import {TestBed} from '@angular/core/testing';
import {PetService} from './pet.service';
import {PetStatus} from "../../../enums/pet-status";
import {HttpClientModule} from "@angular/common/http";

describe('PetService', () => {

  let service: PetService;

  const rex = {
    id: 100,
    category: {
      id: 0,
      name: ""
    },
    name: "Rex",
    photoUrls: [],
    tags: [],
    status: PetStatus.available
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(PetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add a pet to the store', (done: DoneFn) => {
    service.addPet(rex).subscribe(res => {
      expect(res).toEqual(rex);
      done();
    });
  });

  it('should contain previous added pet', (done: DoneFn) => {
    service.getPetsByStatus(PetStatus.available).subscribe(res => {
      expect(res).toContain(rex);
      done();
    });
  });

  it('should get previous added pet', (done: DoneFn) => {
    service.getPetById(rex.id).subscribe(res => {
      expect(res).toEqual(rex);
      done();
    });
  });

  it('should delete previous added pet from collection', (done: DoneFn) => {
    service.removePet(rex.id).subscribe(res => {
      expect(res.code).toEqual(200);
      done();
    });
  });

});
