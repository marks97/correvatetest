import { Injectable } from '@angular/core';
import { HttpService } from "../../http/http.service";
import { PetStatus } from "../../../enums/pet-status";
import { PetDto } from "../../../interfaces/DTO/pet.dto";
import { Observable } from "rxjs";
import { Pet } from "../../../interfaces/domain/pet";
import { Response } from "../../../interfaces/API/response";

@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(
    private httpService: HttpService
  ) { }

  addPet(pet: PetDto): Observable<Pet> {
    return this.httpService.post<Pet>({ path: 'pet', body: pet });
  }

  setPetPhoto(id: number, photo: FormData): Observable<Response> {
    return this.httpService.post({ path: 'pet/' + id + '/uploadImage', body: photo });
  }

  getPetById(id: number): Observable<Pet> {
    return this.httpService.get<Pet>({ path: 'pet/' +  id });
  }

  getPetsByStatus(status: PetStatus): Observable<Pet[]> {
    return this.httpService.get<Pet[]>({ path: 'pet/findByStatus?status=' +  status });
  }

  removePet(id: number): Observable<Response> {
    return this.httpService.delete({ path: 'pet/' +  id });
  }

}
