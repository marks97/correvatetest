import { Injectable } from '@angular/core';
import { PetStatus } from "../../enums/pet-status";

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  constructor() { }

  getStatusColor(status?: PetStatus): { bgColor: string, txtColor: string } {
    switch (status) {
      case PetStatus.available:
        return { bgColor: '#dcfce7', txtColor: '#166534' };
      case PetStatus.pending:
        return { bgColor: '#fef9c3', txtColor: '#ca8a04' };
      case PetStatus.sold:
        return { bgColor: '#fee2e2', txtColor: '#991b1b' };
      default:
        return { bgColor: '', txtColor: '' };
    }
  }

}
