import {Injectable} from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from "@angular/material/dialog";
import {ComponentType} from "@angular/cdk/overlay";

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private matDialog: MatDialog) { }

  public createModal(component: ComponentType<unknown>, panelClass?: string[]): MatDialogRef<unknown> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.id = "modal-component";
    dialogConfig.panelClass = panelClass || ["w-full", "md:h-2/3", "h-5/6"]
    return this.matDialog.open(component, dialogConfig);
  }

}
